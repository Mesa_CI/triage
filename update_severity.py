#!/usr/bin/python

import csv
import datetime
import os
import sys

import gitlab
from issue import Issue

def update_severity():
    gl = gitlab.Gitlab('https://gitlab.freedesktop.org')
    mesa = gl.projects.get(176)
    known_issues = {}

    parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))

    # parse known issues
    last_update = datetime.datetime.fromisoformat("2010-01-01T00:00:00.000+00:00")
    with open(f'{parent_dir}/severity.csv', encoding='utf-8') as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            issue = Issue(from_csv=row)
            known_issues[int(issue.issue)] = issue
            updated = datetime.datetime.fromisoformat(issue.updated.replace('Z', '+00:00'))
            if updated > last_update:
                last_update = updated

    # obtain the last updated starting date

    # iterate issues changed since last update

    open_issues = mesa.issues.list(all=True, updated_after=last_update)
    for gitlab_issue in open_issues:
        iid = gitlab_issue.iid
        if iid not in known_issues:
            issue = Issue(from_gitlab = gitlab_issue)
            known_issues[int(issue.issue)] = issue
        else:
            known_issues[int(iid)].update(gitlab_issue)

    # iterate MRs modified after the previous update

    # update Issues which have an unreviewed MR
    mergerequests = mesa.mergerequests.list(all=True, updated_after=last_update)
    for merge in mergerequests:
        if merge.state == 'closed':
            continue
        merge_id = str(merge.iid)
        for issue in merge.closes_issues():
            issue_id = issue.iid
            issue = known_issues[issue_id]
            if issue.state != 'Closed':
                # this issue has an unreviewed fix, and triage must be
                # done to assign to a reviewer.
                issue.state = "NeedsReview"
                if not issue.fixes:
                    issue.fixes = merge_id
                elif merge_id not in issue.fixes:
                    issue.fixes = f"{issue.fixes}, {merge_id}"

    with open(f'{parent_dir}/severity.csv', "w", encoding='utf-8') as fh:
        writer = csv.DictWriter(fh, fieldnames=['issue', 'severity', 'created', 'triaged', 'updated',
                                                'closed', 'author', 'culprit', 'assignee', 'labels',
                                                'flags', 'state', 'fixes', 'title', 'gitlab_state'])
        writer.writeheader()
        # sort issues
        issue_nums = sorted(known_issues.keys())
        issue_nums.reverse()
        for k in issue_nums:
            writer.writerow(known_issues[k].__dict__)

# write the time of the current update

if __name__ == '__main__':
    update_severity()
