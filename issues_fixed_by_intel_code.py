#!/usr/bin/python

import csv
import datetime
import os
import sys

import update_severity
from issue import Issue
import utils


def intel_dev_issues_found(start_date, end_date):
    current_issues = []
    parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))

    # update severity csv first
    update_severity.update_severity()

    # parse fixed issues
    with open(f'{parent_dir}/severity.csv', encoding='utf-8') as fh:
        reader = csv.DictReader(fh)
        for row in reader:
            issue = Issue(from_csv=row)
            try:
                closed = (datetime.
                          datetime.
                          fromisoformat(issue.closed.replace('Z', '+00:00')))
            except ValueError:
                continue

            if (closed.date() >= start_date and
                closed.date() <= end_date and
                issue.flags != "NotOurBug" and
                issue.state == "Closed" and
                issue.assignee.lower() in utils.intel_devs()):
                current_issues.append(issue)

    with open(f'{parent_dir}/issues_fixed_by_intel_code.csv',
              "w",
              encoding='utf-8') as fh:
        writer = csv.DictWriter(fh, delimiter='\t',
                                fieldnames=['issue number', 'labels'])
        writer.writeheader()

        for i in current_issues:
            writer.writerow({'issue number': i.issue,
                             'labels': i.labels})


if __name__ == '__main__':
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print('usage: python3 issues_fixed_by_intel_code.py '
              '<start_date> <end_date>')
        exit(0)

    start_date = sys.argv[1]
    end_date = sys.argv[2]
    intel_dev_issues_found(datetime.date.fromisoformat(start_date),
                           datetime.date.fromisoformat(end_date))
