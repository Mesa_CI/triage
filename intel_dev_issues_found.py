#!/usr/bin/python

import csv
import datetime
import os
import sys

import update_severity
from issue import Issue
import utils


def intel_dev_issues_found(start_date, end_date):
    known_issues = {}
    parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
    issues_per_dev = {}

    for d in utils.intel_devs():
        issues_per_dev[d] = {'issue_count': 0, 'issue_numbers': []}

    # update severity csv first
    update_severity.update_severity()

    # parse known issues
    with open(f'{parent_dir}/severity.csv', encoding='utf-8') as fh:
        reader = csv.DictReader(fh)
        for row in reader :
            issue = Issue(from_csv=row)
            created = (datetime.
                       datetime.
                       fromisoformat(issue.created.replace('Z', '+00:00')))

            if created.date() >= start_date and created.date() <= end_date:
                known_issues[int(issue.issue)] = issue

    with open(f'{parent_dir}/intel_dev_issues_found.csv',
              "w",
              encoding='utf-8') as fh:
        writer = csv.DictWriter(fh, delimiter='\t',
                                fieldnames=['developer',
                                            'issues found from '
                                            f'{start_date} to {end_date}',
                                            'issue numbers'])
        writer.writeheader()

        for v in known_issues.values():
            try:
                issues_per_dev[v.author]['issue_count'] += 1
                issues_per_dev[v.author]['issue_numbers'].append(v.issue)
            except KeyError:
                continue

        for k, v in sorted(issues_per_dev.items(), key=lambda i: i[0]):
            writer.writerow({'developer': k,
                             'issues found from '
                             f'{start_date} to {end_date}': v['issue_count'],
                             'issue numbers': ' '.join(v['issue_numbers'])})


if __name__ == '__main__':
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print('usage: python3 inteldev_issues_found.py'
              '<start_date> <end_date>')

    start_date = sys.argv[1]
    end_date = sys.argv[2]
    intel_dev_issues_found(datetime.date.fromisoformat(start_date),
                           datetime.date.fromisoformat(end_date))
