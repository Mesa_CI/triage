#!/usr/bin/python

import csv
import datetime
import os
import sys

import gitlab
import utils


def intel_mrs_submitted(start_date, end_date):
    gl = gitlab.Gitlab('https://gitlab.freedesktop.org')
    mesa = gl.projects.get(176)
    parent_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
    mrs_per_dev = {}

    for d in utils.intel_devs():
        mrs_per_dev[d] = {'mr_count': 0, 'mr_numbers': []}

    with open(f'{parent_dir}/intel_mrs_submitted.csv', "w", encoding='utf-8') as fh:
        mrs = mesa.mergerequests.list(all=True, created_after=start_date)
        writer = csv.DictWriter(fh, delimiter='\t',
                                fieldnames=['developer',
                                            'mrs submitted from '
                                            f'{start_date} to {end_date}',
                                            'mr numbers'])
        writer.writeheader()

        for mr in mrs:
            try:
                mrs_per_dev[mr.author['username']]['mr_count'] += 1
                (mrs_per_dev[mr.author['username']]
                 ['mr_numbers']).append(str(mr.iid))
            except KeyError:
                continue

        for k, v in sorted(mrs_per_dev.items(), key=lambda i: i[0]):
            writer.writerow({'developer': k,
                             'mrs submitted from '
                             f'{start_date} to {end_date}': v['mr_count'],
                             'mr numbers': ' '.join(v['mr_numbers'])})


if __name__ == '__main__':
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print('usage: python3 intel_mrs_submitted'
              '<start_date> <end_date>')

    start_date = sys.argv[1]
    end_date = sys.argv[2]
    intel_mrs_submitted(datetime.date.fromisoformat(start_date),
                        datetime.date.fromisoformat(end_date))
